-- Configuration
function love.conf(t)
	t.title = "Breakout"
	t.version = "0.10.1"
	t.window.width = 768
	t.window.height = 1024

	-- For Windows debugging
	t.console = false
end
