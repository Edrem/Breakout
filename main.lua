Class = require 'libs/middleclass'
Flux = require "libs/flux"
require "libs/slam";

require "objects/"
require "Rooms"

Game = {Title = "", FPS = 60, BackgroundColour = {24, 24, 24}};
Game_Directory = love.filesystem.getWorkingDirectory();
pointScore = 100;
MultiBallTimerX = nil;
MultiBallTimerY = nil;

-- Because screw American spelling
love.graphics.setColour = love.graphics.setColor;
Game.BackgroundColor = Game.BackgroundColour;

View = {};
View.x = 0;
View.y = 0;

function beginContact(a, b, coll)
end
function endContact(a, b, coll)

end
function preSolve(a, b, coll)

end
function postSolve(a, b, coll, normalimpulse, tangentimpulse)
    ball = a;
    other = b;
    if b:getUserData() then
        if b:getUserData().ball then
            ball = b;
            other = a;
        end
    end
    if other:getUserData() then
        local d = other:getUserData()
        ball:getUserData().object.scale = 1.25;
        Flux.to(ball:getUserData().object, 0.1, {scale = 1});
        if d.type == "brick" then
            local ballObject = ball:getUserData().object;
            ballObject.colourTimer = 1;
            ballObject.colour = d.object.colour;
            instance_destroy(d.object);
            table.insert(objList, PointScore:new(ballObject.ball.body:getX(), ballObject.ball.body:getY() - 80, pointScore));
            pointScore = pointScore * 2;
            pointScore = math.min(3200, pointScore);
            brickCount = brickCount - 1;
            if brickCount < 0 then
                table.insert(objList, Congratulations:new());
            end
        end
        if d.type == "paddle" then
            pointScore = 100;
            paddlesound:play();
            d.object.scale = 1.25;
            Flux.to(d.object, 0.1, {scale = 1});
        end
        if d.type == "hardbrick" then
            hithardsound:play();
        end
        local timer = 0.15;
        local offset = -8;
        if d.leftWall then
            View.x = 0;
            Flux.to(View, timer, {x = offset}):ease("quadout"):after( timer*2, {x = -offset}):ease("quadout"):after( timer*1.5, {x = 0}):ease("quadout")
            wallsound:play();
        end
        if d.rightWall then
            offset = -offset;
            View.x = 0;
            Flux.to(View, timer, {x = offset}):ease("quadout"):after( timer*2, {x = -offset}):ease("quadout"):after( timer*1.5, {x = 0}):ease("quadout")
            wallsound:play();
        end
        if d.topWall then
            offset = -offset;
            View.y = 0;
            Flux.to(View, timer, {y = offset}):ease("quadout"):after( timer*2, {y = -offset}):ease("quadout"):after( timer*1.5, {y = 0}):ease("quadout")
            wallsound:play();
        end
    end
end

function love.load(arg)
    min_dt = 1/Game.FPS;
    next_time = love.timer.getTime();

    love.physics.setMeter(10);
    world = love.physics.newWorld(0, 0, true);
    world:setCallbacks(beginContact, endContact, preSolve, postSolve);

    SplatCanvas = love.graphics.newCanvas(love.graphics.getWidth(), love.graphics.getHeight());
    Splash = {};
    --[[
    ImageLoader:loadImage("images/splat1.png", Splash, "splat1");
    ImageLoader:loadImage("images/splat2.png", Splash, "splat2");
    ImageLoader:loadImage("images/splat3.png", Splash, "splat3");
    ImageLoader:loadImage("images/splat4.png", Splash, "splat4");
    ImageLoader:loadImage("images/splat5.png", Splash, "splat5");
    ImageLoader:loadImage("images/splat6.png", Splash, "splat6");
    ImageLoader:loadImage("images/splat7.png", Splash, "splat7");
    ]]
    Splash.splat1 = love.graphics.newImage("images/splat1.png");
    Splash.splat2 = love.graphics.newImage("images/splat2.png");
    Splash.splat3 = love.graphics.newImage("images/splat3.png");
    Splash.splat4 = love.graphics.newImage("images/splat4.png");
    Splash.splat5 = love.graphics.newImage("images/splat5.png");
    Splash.splat6 = love.graphics.newImage("images/splat6.png");
    Splash.splat7 = love.graphics.newImage("images/splat7.png");

    blockSprite = love.graphics.newImage("images/brick.png");
    blockHardSprite = love.graphics.newImage("images/brickhard.png");
    blockBrokenSprite = love.graphics.newImage("images/brickbroken.png");
    powerupSprite = love.graphics.newImage("images/powerup.png");
    multiballSprite = love.graphics.newImage("images/multiball.png");
    tile = love.graphics.newImage("images/tile.png");
    pointFont = love.graphics.newFont("fonts/kenpixel.ttf", 80);
    ballSprite = love.graphics.newImage("images/ball.png");

    splatsound = love.audio.newSource("audio/splat.ogg", "static");
    hithardsound = love.audio.newSource("audio/hithard.ogg", "static");
    paddlesound = love.audio.newSource("audio/laser2.ogg", "static")
    wallsound = love.audio.newSource("audio/wallhit.ogg", "static")
    music = love.audio.newSource("audio/cycles.mp3");
    music:setLooping(true);
    music:setVolume(0.6);
    music:play();

    backgroundGlow = BackgroundGlow:new();
    backgroundGear = BackgroundGear:new();

    gate = Gate:new();
    gate.progress = 1;
    Flux.to(gate, 0.5, {progress = 0}):ease("linear"):delay(0.25):oncomplete(finishGaate);
    Rooms:MainMenu();
end

-- Updating
function love.update(dt)
    dt = math.min(dt, 2/60);
    dt = math.max(1/60, dt);
    Flux.update(dt);
    world:update( dt);
    ImageLoader:_update();

    if masterObj then
        MultiBallTimerX = nil;
    end
    if MultiBallTimerX then
        local x = MultiBallTimerX;
        local y = MultiBallTimerY;
        MultiBallTimerX = nil;
        local ball = Ball:new(x, y, false);
        local angle_x = math.cos(math.rad(math.random(360))) * 2500;
		local angle_y =-math.sin(math.rad(math.random(360))) * 2500;
		ball.ball.body:applyLinearImpulse(angle_x, angle_y);
        table.insert(objList, ball);
    end

    next_time = next_time + min_dt;
    if love.keyboard.isDown('escape') then
        love.event.push('quit')
    end

    if masterObj then
        masterObj:update(dt);
    else
        for key, value in pairs(objList) do
            value:update(dt);
        end
    end
end

function love.draw()
    love.graphics.clear( Game.BackgroundColor);
    love.graphics.reset();

    backgroundGlow:draw();
    backgroundGear:draw();

    love.graphics.setColour({200, 200, 200});
    love.graphics.draw(SplatCanvas);
    love.graphics.translate(View.x, View.y);
    love.graphics.setColour({255, 255, 255});

    -- Draw
    for i = -1, 3, 1 do
        for key, value in pairs(objList) do
            if value.depth == i then
                love.graphics.translate(View.x, View.y);
                value:draw();
                love.graphics.reset();
            end
        end
    end

    -- Draw GUI
   for key, value in pairs(objList) do
       value:drawGui();
       love.graphics.reset();
   end
   gate:drawGui();

    local cur_time = love.timer.getTime();
    if next_time <= cur_time then
        next_time = cur_time;
        return;
    end
    love.timer.sleep(next_time - cur_time);
end

function love.keypressed( key )
    if masterObj then
        masterObj:keypressed(key);
    else
        for k, value in pairs(objList) do
            value:keypressed(key);
        end
    end
end
function love.keyreleased( key )
    if masterObj then
        masterObj:keyreleased(key);
    else
        for k, value in pairs(objList) do
            value:keyreleased(key);
        end
    end
end
function love.mousepressed( x, y, button, isTouch )
    if masterObj then
        masterObj:mousepressed( x, y, button, isTouch );
    else
        for k, value in pairs(objList) do
            value:mousepressed( x, y, button, isTouch );
        end
    end
end
function love.mousereleased( x, y, button, isTouch )
    if masterObj then
        masterObj:mousereleased( x, y, button, isTouch );
    else
        for k, value in pairs(objList) do
            value:mousereleased( x, y, button, isTouch );
        end
    end
end
function love.wheelmoved( x, y)
    if masterObj then
        masterObj:wheelmoved( x, y);
    else
        for k, value in pairs(objList) do
            value:wheelmoved( x, y);
        end
    end
end
