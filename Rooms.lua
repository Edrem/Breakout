Rooms = {};

function Rooms:MainMenu()
	object_destroy(GameObject);
	table.insert(objList, MainMenu:new());
end

local colours = {};
local function generateColours()
	local colourType = math.floor(math.random(8));

	if colourType == 0 then
		-- Shades of red
		colours[1] = {255, 128, 128};
		colours[2] = {255, 64, 64};
		colours[3] = {255, 0, 0};
		colours[4] = {225, 0, 0};
		colours[5] = {195, 0, 0};
		colours[6] = {175, 0, 0};
		colours[7] = {155, 0, 0};
	end

	if colourType == 2 then
		-- Shades of blue
		colours[1] = {128, 128, 255};
		colours[2] = {64, 64, 255};
		colours[3] = { 0, 0, 255};
		colours[4] = { 0, 0, 225};
		colours[5] = { 0, 0, 195};
		colours[6] = { 0, 0, 175};
		colours[7] = { 0, 0, 155};
	end

	if colourType == 3 then
		-- Shades of green
		colours[1] = {128, 255, 128};
		colours[2] = {64, 255, 64};
		colours[3] = { 0, 255, 0};
		colours[4] = { 0, 225, 0};
		colours[5] = { 0, 195, 0};
		colours[6] = { 0, 175, 0};
		colours[7] = { 0, 155, 0};
	end

	if colourType == 4 then
		-- Rainbow
		colours[1] = { 255, 0, 0};
		colours[2] = { 255, 128, 0};
		colours[3] = { 255, 255, 0};
		colours[4] = { 0, 255, 0};
		colours[5] = { 0, 255, 255};
		colours[6] = { 0, 0, 255};
		colours[7] = { 255, 0, 255};
	end

	if colourType == 5 then
		-- Purple
		colours[1] = {255, 128, 255};
		colours[2] = {255, 64, 255};
		colours[3] = { 255, 0, 255};
		colours[4] = { 225, 0, 225};
		colours[5] = { 195, 0, 195};
		colours[6] = { 175, 0, 175};
		colours[7] = { 155, 0, 155};
	end
	if colourType == 6 then
		-- Yellow
		colours[1] = {255, 255, 128};
		colours[2] = {255, 255, 64};
		colours[3] = { 255, 255, 0};
		colours[4] = { 225, 225, 0};
		colours[5] = { 195, 195, 0};
		colours[6] = { 175, 175, 0};
		colours[7] = { 155, 155, 0};
	end
	if colourType == 7 then
		-- Cyan
		colours[1] = {128, 255, 255};
		colours[2] = {64, 255, 255};
		colours[3] = { 0, 255, 255};
		colours[4] = { 0, 225, 225};
		colours[5] = { 0, 195, 195};
		colours[6] = { 0, 175, 175};
		colours[7] = { 0, 155, 155};
	end
end

function Rooms:LevelOne()
	object_destroy(GameObject);
	table.insert(objList, Paddle:new());
	table.insert(objList, WallBounds:new());
	table.insert(objList, Ball:new(0, 0, true));

	-- Add in all the bricks
	local gameWidth = love.graphics.getWidth() - 160;
    local gameHeight = love.graphics.getHeight() - 32;

    local rows = math.floor(gameWidth / 60) - 1;
    local columns = 4;
    local xoff = (love.graphics.getWidth() - rows*60)/2;
    local yoff = 160;

	generateColours();
	local sound = love.audio.newSource("audio/level1.ogg");
	sound:play();

	local brick;
    for y = 0, columns-1 do
        for x = 0, rows-1 do
			--[[
			if x == 0 or x == rows-1 or y == columns-1 then
	            brick = BrickHard:new(xoff + x * 60, yoff + y * 40, colours[y+1], "elastic" );
			else
				brick = Brick:new(xoff + x * 60, yoff + y * 40, colours[y+1], "elastic" );
			end
			]]
			brick = Brick:new(xoff + x * 60, yoff + y * 40, colours[y+1], "elastic" );
            table.insert(objList, brick);
        end
    end

	ballCount = 1;
	brickCount = columns * rows;
	print(brickCount);
	nextLevel = "LevelTwo";
end

function Rooms:LevelTwo()
	object_destroy(GameObject);
	table.insert(objList, Paddle:new());
	table.insert(objList, WallBounds:new());
	table.insert(objList, Ball:new(0, 0, true));

	-- Add in all the bricks
	local gameWidth = love.graphics.getWidth() - 160;
    local gameHeight = love.graphics.getHeight() - 32;

    local rows = math.floor(gameWidth / 60) - 1;
    local columns = 7;
    local xoff = (love.graphics.getWidth() - rows*60)/2;
    local yoff = 160;

	generateColours();
	local sound = love.audio.newSource("audio/level2.ogg");
	sound:play();

	local brick;
    for y = 0, columns-1 do
        for x = 0, rows-1 do
			chance = math.floor(math.random(6));
			if chance == 1 then
				chance = math.floor(math.random(2));
				if chance == 1 then
					brick = BrickMultiBall:new(xoff + x * 60, yoff + y * 40, colours[y+1], "elastic" );
				else
					--brick = BrickPowerUp:new(xoff + x * 60, yoff + y * 40, colours[y+1], "elastic" );
					brick = BrickMultiBall:new(xoff + x * 60, yoff + y * 40, colours[y+1], "elastic" );
				end
			else
				brick = Brick:new(xoff + x * 60, yoff + y * 40, colours[y+1], "elastic" );
			end
            table.insert(objList, brick);
        end
    end

	ballCount = 1;
	brickCount = columns * rows;
	nextLevel = "LevelThree";
end

function Rooms:LevelThree()
	object_destroy(GameObject);
	table.insert(objList, Paddle:new());
	table.insert(objList, WallBounds:new());
	table.insert(objList, Ball:new(0, 0, true));

	-- Add in all the bricks
	local gameWidth = love.graphics.getWidth() - 160;
    local gameHeight = love.graphics.getHeight() - 32;

    local rows = math.floor(gameWidth / 60) - 1;
    local columns = 7;
    local xoff = (love.graphics.getWidth() - rows*60)/2;
    local yoff = 160;

	generateColours();
	local sound = love.audio.newSource("audio/level3.ogg");
	sound:play();

	local brick;
    for y = 0, columns-1 do
        for x = 0, rows-1 do
			if x == 0 or x == rows-1 then
	            brick = BrickHard:new(xoff + x * 60, yoff + y * 40, colours[y+1], "elastic" );
			else
				chance = math.floor(math.random(6));
				if chance == 1 then
					chance = math.floor(math.random(2));
					if chance == 1 then
						brick = BrickMultiBall:new(xoff + x * 60, yoff + y * 40, colours[y+1], "elastic" );
					else
						--brick = BrickPowerUp:new(xoff + x * 60, yoff + y * 40, colours[y+1], "elastic" );
						brick = BrickMultiBall:new(xoff + x * 60, yoff + y * 40, colours[y+1], "elastic" );
					end
				else
					brick = Brick:new(xoff + x * 60, yoff + y * 40, colours[y+1], "elastic" );
				end
			end
            table.insert(objList, brick);
        end
    end

	ballCount = 1;
	brickCount = (columns) * (rows-2);
	print(brickCount);
	nextLevel = "LevelFour";
end


function Rooms:LevelThree()
	object_destroy(GameObject);
	table.insert(objList, Paddle:new());
	table.insert(objList, WallBounds:new());
	table.insert(objList, Ball:new(0, 0, true));

	-- Add in all the bricks
	local gameWidth = love.graphics.getWidth() - 160;
    local gameHeight = love.graphics.getHeight() - 32;

    local rows = math.floor(gameWidth / 60) - 1;
    local columns = 7;
    local xoff = (love.graphics.getWidth() - rows*60)/2;
    local yoff = 160;

	generateColours();
	local sound = love.audio.newSource("audio/level3.ogg");
	sound:play();

	local brick;
    for y = 0, columns-1 do
        for x = 0, rows-1 do
			if x == 0 or x == rows-1 then
	            brick = BrickHard:new(xoff + x * 60, yoff + y * 40, colours[y+1], "elastic" );
			else
				chance = math.floor(math.random(6));
				if chance == 1 then
					chance = math.floor(math.random(2));
					if chance == 1 then
						brick = BrickMultiBall:new(xoff + x * 60, yoff + y * 40, colours[y+1], "elastic" );
					else
						--brick = BrickPowerUp:new(xoff + x * 60, yoff + y * 40, colours[y+1], "elastic" );
						brick = BrickMultiBall:new(xoff + x * 60, yoff + y * 40, colours[y+1], "elastic" );
					end
				else
					brick = Brick:new(xoff + x * 60, yoff + y * 40, colours[y+1], "elastic" );
				end
			end
            table.insert(objList, brick);
        end
    end

	ballCount = 1;
	brickCount = (columns) * (rows-2);
	print(brickCount);
	nextLevel = "LevelFour";
end

function Rooms:LevelFour()
	object_destroy(GameObject);
	table.insert(objList, Paddle:new());
	table.insert(objList, WallBounds:new());
	table.insert(objList, Ball:new(0, 0, true));

	-- Add in all the bricks
	local gameWidth = love.graphics.getWidth() - 160;
    local gameHeight = love.graphics.getHeight() - 32;

    local rows = math.floor(gameWidth / 60) - 1;
    local columns = 7;
    local xoff = (love.graphics.getWidth() - rows*60)/2;
    local yoff = 160;

	generateColours();
	local sound = love.audio.newSource("audio/level4.ogg");
	sound:play();

	local brick;
    for y = 0, columns-1 do
        for x = 0, rows-1 do
			brick = BrickMultiBall:new(xoff + x * 60, yoff + y * 40, colours[y+1], "elastic" );
            table.insert(objList, brick);
        end
    end

	ballCount = 1;
	brickCount = (columns) * (rows);
	print(brickCount);
	nextLevel = "MainMenu";
end
