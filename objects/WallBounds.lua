WallBounds = Class("WallBounds", GameObject)

function WallBounds:initialize()
	-- Create the outer bounds area
    local size = 16;
    local gameWidth = love.graphics.getWidth();
    local gameHeight = love.graphics.getHeight();

    ground2 = {}
    ground2.body = love.physics.newBody(world, gameWidth/2, size/2)
    ground2.shape = love.physics.newRectangleShape(gameWidth, size);
    ground2.fixture = love.physics.newFixture(ground2.body, ground2.shape)
    ground2.fixture:setUserData({topWall = true});

    ground3 = {}
    ground3.body = love.physics.newBody(world, size/2, gameHeight/2)
    ground3.shape = love.physics.newRectangleShape(size, gameHeight);
    ground3.fixture = love.physics.newFixture(ground3.body, ground3.shape)
    ground3.fixture:setUserData({leftWall = true});

    ground4 = {}
    ground4.body = love.physics.newBody(world, gameWidth-size/2, gameHeight/2)
    ground4.shape = love.physics.newRectangleShape(size, gameHeight);
    ground4.fixture = love.physics.newFixture(ground4.body, ground4.shape)
    ground4.fixture:setUserData({rightWall = true});
end

function WallBounds:destroy()
	ground2.body:destroy();
	ground3.body:destroy();
	ground4.body:destroy();
end


function WallBounds:draw()
    love.graphics.polygon("fill", ground2.body:getWorldPoints(ground2.shape:getPoints()))
    love.graphics.polygon("fill", ground3.body:getWorldPoints(ground3.shape:getPoints()))
    love.graphics.polygon("fill", ground4.body:getWorldPoints(ground4.shape:getPoints()))
end
