BrickPowerUp = Class("BrickPowerUp", GameObject)

function BrickPowerUp:initialize(x, y, colour, ease)
	Brick.initialize(self, x, y, colour, ease);
end

function BrickPowerUp:destroy()
	Brick.destroy(self);
end

function BrickPowerUp:draw()
	Brick.draw(self);
	love.graphics.setColor(255, 255, 255);
	love.graphics.draw(powerupSprite, self.x + self.width/2, self.y + self.offset + self.height/2, math.rad(self.angle), self.scale, self.scale, powerupSprite:getWidth()/2, powerupSprite:getHeight()/2);
end
