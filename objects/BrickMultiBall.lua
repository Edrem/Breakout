BrickMultiBall = Class("BrickMultiBall", GameObject)

function BrickMultiBall:initialize(x, y, colour, ease)
	Brick.initialize(self, x, y, colour, ease);
end

function BrickMultiBall:destroy()
	if not masterObj then
		MultiBallTimerX = self.x + self.width/2;
		MultiBallTimerY = self.y + self.height/2;
		ballCount = ballCount + 1;
	end
	Brick.destroy(self);
end

function BrickMultiBall:draw()
	Brick.draw(self);
	love.graphics.setColor(255, 255, 255);
	love.graphics.draw(multiballSprite, self.x + self.width/2, self.y + self.offset + self.height/2, math.rad(self.angle), self.scale, self.scale, powerupSprite:getWidth()/2, powerupSprite:getHeight()/2);
end
