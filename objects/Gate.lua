Gate = Class("Gate", GameObject)

local function loadRoom(obj)
	Rooms[obj.room]();
	masterObj = nil;
	splatsound:play();
end

function finishGaate(obj)
	masterObj = nil;
end

function Gate:initialize(room)
	GameObject:initialize();
	self.gateTop = love.graphics.newImage("images/gatetop.png");
	self.gateBot = love.graphics.newImage("images/gatebottom.png");
	self.progress = 0;
	masterObj = self;
end

function Gate:gotoRoom(room)
	self.progress = 0;
	self.room = room;
	Flux.to(self, 0.5, {progress = 1}):ease("linear"):oncomplete(loadRoom):after(0.5, {progress = 0}):ease("linear"):delay(0.25):oncomplete(finishGaate);
end

function Gate:drawGui()
	love.graphics.draw(self.gateTop, 0, -256 + self.progress * (768 - self.gateTop:getHeight()/2))
	love.graphics.draw(self.gateBot, 0, (love.graphics.getHeight() + 256) - self.gateBot:getHeight() - (self.progress * (768 - self.gateBot:getHeight()/2)))
	love.graphics.setColour(150, 150, 150);
	love.graphics.rectangle("fill", 0, 0, love.graphics.getWidth(), -256 + self.progress * (768 - self.gateTop:getHeight()/2));
	love.graphics.rectangle("fill", 0, (love.graphics.getHeight() + 256) - self.progress * (768 - self.gateTop:getHeight()/2), love.graphics.getWidth(), love.graphics.getHeight()/2);
end
