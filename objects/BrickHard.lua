BrickHard = Class("BrickHard", GameObject)

function BrickHard:initialize(x, y, colour, ease)
	Brick.initialize(self, x, y, colour, ease);
	self.fixture:setUserData({type = "hardbrick"});
end

function BrickHard:destroy()
	Brick.destroy(self);
end

function BrickHard:draw()
	love.graphics.setColor(128, 128, 128);
	love.graphics.draw(blockHardSprite, self.x + self.width/2, self.y + self.offset + self.height/2, 0, 1, 1, blockHardSprite:getWidth()/2, blockHardSprite:getHeight()/2);
end
