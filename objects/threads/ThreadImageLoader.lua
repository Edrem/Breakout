require "love.image"
require "love.filesystem"

game_directory = love.filesystem.getWorkingDirectory();

img = select(1, ...);

print("Loading " .. game_directory .. "/" .. img);
file = io.open(game_directory .. "/" .. img, "rb");
contents = file:read("*all");
file:close();

image = love.filesystem.newFileData(contents, img, "file");

channel = love.thread.getChannel("imageChannel");
channel:push( {image , img});
