MainMenu = Class("MainMenu", GameObject)

function MainMenu:initialize()
	GameObject:initialize();
	self.x = 40;
	self.y = 40;
	self.pos = 0;
	self.menu = {"Start", "Exit"};
	self.font = love.graphics.newFont("fonts/kenpixelblocks.ttf", 80);
	self.font2 = love.graphics.newFont(18);
	love.graphics.setCanvas(SplatCanvas);
	love.graphics.clear();
	love.graphics.setCanvas();
end

function MainMenu:draw()
	love.graphics.setFont(self.font);
	love.graphics.setColour({255, 255, 255});
	love.graphics.print("BREAKOUT", self.x, self.y);

	love.graphics.setLineWidth(8);
	for i = 0, 1 do
		love.graphics.setColour(128, 128, 128);
		if self.pos == i then
			love.graphics.setColour(255, 255, 255);
			love.graphics.rectangle("line", self.x - 14, self.y + 820 + 75*i, 300, 60);
		end
		love.graphics.print(self.menu[i+1], self.x, self.y + 820 + 75*i, 0, 0.5, 0.5);
	end
	love.graphics.setColour(200, 200, 200);
	love.graphics.setFont(self.font2);
	love.graphics.printf("Made for the 2016 Brick Breaker Jam", 0, self.y + 910, love.graphics.getWidth() - 15, "right");
	love.graphics.printf("by Joshua (Edrem) Bax", 0, self.y + 930, love.graphics.getWidth() - 15, "right");
	love.graphics.printf("music by audionautix.com", 0, self.y + 950, love.graphics.getWidth() - 15, "right");
end

function MainMenu:keypressed(key)
	if key == "up" then
		self.pos = math.max(0, self.pos - 1);
	end
	if key == "down" then
		self.pos = math.min(self.pos + 1, 1)
	end
	if key == "return" or key == "space" then
		if self.pos == 0 then
			gate:gotoRoom("LevelOne");
		end
		if self.pos == 1 then
			love.event.push("quit");
		end
	end
end
