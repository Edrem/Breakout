local current_folder = (...):gsub('%.init$', '')
require (current_folder .. "ImageLoader")
require (current_folder .. "Util")
