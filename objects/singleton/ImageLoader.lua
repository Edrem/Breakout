local ImageLoaderSingleton = Class('ImageLoader');

local images = {};
local callback = {};
local imageThread = nil;
local channel;

function ImageLoaderSingleton:initialize()
	channel = love.thread.getChannel("imageChannel");
end

function ImageLoaderSingleton:_update()
	value = channel:pop();
    if value then
        img = love.graphics.newImage(value[1]);
		file = value[2];
		for k, v in pairs(callback[file]) do
			v.obj[v.var] = img;
		end
		print("Finished loading image")
		callback[file] = nil;
		images[file] = img;
    end
end

function ImageLoaderSingleton:loadImage(file, object, variable)
    if images[file] then
		object[variable] = images[file];
		return;
    end

	if not callback[file] then
		callback[file] = {};
		imageThread = love.thread.newThread("objects/threads/ThreadImageLoader.lua");
		imageThread:start(file);
	end
	table.insert(callback[file], {obj = object, var = variable});
end

ImageLoader = ImageLoaderSingleton:new();
