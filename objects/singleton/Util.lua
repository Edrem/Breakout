local UtilSingleton = Class('Util');

function UtilSingleton:initialize()
end

function UtilSingleton:mixColour(colour1, colour2, percent)
	tmp = {};
	tmp[1] = colour1[1] * (1-percent) + colour2[1] * percent
	tmp[2] = colour1[2] * (1-percent) + colour2[2] * percent
	tmp[3] = colour1[3] * (1-percent) + colour2[3] * percent
	return tmp;
end

function UtilSingleton:mergeTables(original, new)
	for k,v in pairs(new) do
        original[k] = v;
    end
end

function shallowCopy(original)
	local copy = {};
    for k, v in pairs(original) do
        copy[k] = v;
    end
    return copy;
end

function deepCopy(orig)
    local orig_type = type(orig)
    local copy
    if orig_type == 'table' then
        copy = {}
        for orig_key, orig_value in next, orig, nil do
            copy[deepCopy(orig_key)] = deepCopy(orig_value)
        end
        setmetatable(copy, deepCopy(getmetatable(orig)))
    else -- number, string, boolean, etc
        copy = orig
    end
    return copy
end

Util = UtilSingleton:new();
