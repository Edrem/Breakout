Brick = Class('Brick', GameObject);

local function createTween(obj)
	if obj.offset == obj.maxOffset then
		Flux.to(obj, 1, {offset = -obj.maxOffset}):ease("quadinout"):oncomplete(createTween);
	else
		Flux.to(obj, 1, {offset = obj.maxOffset}):ease("quadinout"):oncomplete(createTween);
	end
end

local function brickTweenScale(obj)
	if obj.scale == obj.maxScale then
		Flux.to(obj, 0.1, {scale = 1}):oncomplete(brickTweenScale);
	else
		Flux.to(obj, 0.1, {scale = obj.maxScale}):delay(0.20):oncomplete(brickTweenScale);
	end
end
local function brickTweenAngle(obj)
	if obj.angle == obj.maxAngle then
		Flux.to(obj, 0.1, {angle = -obj.maxAngle}):delay(0.3):oncomplete(brickTweenAngle);
	else
		Flux.to(obj, 0.1, {angle = obj.maxAngle}):delay(0.3):oncomplete(brickTweenAngle);
	end
end

function Brick:initialize(x, y, colour, ease)
	GameObject:initialize();
	self.x = x;
	self.y = y;
	self.width = 60;
	self.height = 40;
	self.scale = 1;
	self.maxAngle = 3;
	self.angle = self.maxAngle;
	self.maxScale = 1.1;
	self.colour = colour;
    self.body = love.physics.newBody(world, self.x + self.width/2, self.y + self.height/2);
    self.shape = love.physics.newRectangleShape(self.width, self.height);
    self.fixture = love.physics.newFixture(self.body, self.shape);
	self.fixture:setUserData({object = self, type = "brick"});
	self.maxOffset = 1;
	self.offset = -self.maxOffset;
	self.x = -200 + math.random(love.graphics.getWidth()+400);
	self.y = -(40 + math.random(120));
	brickTweenScale(self);
	brickTweenAngle(self);
	Flux.to(self, 1, {offset = self.maxOffset}):ease("quadinout"):delay(math.random(300)/100):oncomplete(createTween);
	if ease == "elastic" then
		Flux.to(self, 1, {x = x, y = y}):ease("elasticout"):delay(1);
	end
	if ease == "normal" then
		self.y = y - 800;
		self.x = x;
		Flux.to(self, 1, {x = x, y = y}):delay(1);
	end
end

function Brick:destroy()
	if not masterObj then
		table.insert(objList, BrickDeath:new(self.x, self.y, self.width, self.height, self.colour));
	end
	self.body:destroy();
end

function Brick:draw()
	love.graphics.setColour(self.colour);
	love.graphics.draw(blockSprite, self.x + self.width/2, self.y + self.offset + self.height/2, math.rad(self.angle), self.scale, self.scale, blockSprite:getWidth()/2, blockSprite:getHeight()/2);
end
