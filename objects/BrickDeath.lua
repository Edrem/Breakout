BrickDeath = Class('BrickDeath', GameObject);

function BrickDeath:initialize(x, y, width, height,colour)
	GameObject:initialize();
	self.x = x;
	self.y = y;
	self.width = width;
	self.height = height;
	self.colour = shallowCopy(colour);
	self.timer = 5;
	self.maxEnlarge = 0.5;
	self.depth = 1;
	Flux.to(self, 7/60, {timer = 0}):ease("quadinout"):oncomplete(BrickDeadCallBack);
end

function BrickDeadCallBack(obj)
	local objs = object_find(BrickDeath);
	for k, v in pairs(objs) do
		if v.timer <= 0 then
			instance_destroy(v);
		end
	end
end

function BrickDeath:destroy()
	love.graphics.setCanvas(SplatCanvas);
	love.graphics.setColour( self.colour);
	for i = 0, 2 do
		local x = (self.x + self.width/2) - 10 + math.random(20);
		local y = (self.y + self.height/2) - 10 + math.random(20);
		local angle = math.random(360);
		local image = math.floor(math.random(table.getn(Splash)) + 1);
		love.graphics.draw(Splash["splat"..image], x, y, angle, 1.25, 1.25, 64, 64);
	end
	love.graphics.setCanvas();
	splatsound:play();
end

function BrickDeath:draw()
	local progress = 1 - (self.timer/5);
	local newCol = Util:mixColour(self.colour, {255, 255, 255}, progress/2);
	local w = self.width * (1 + self.maxEnlarge * progress);
	local h = self.height * (1 + self.maxEnlarge * progress);
	local x = self.x - (w - self.width)/2;
	local y = self.y - (h - self.height)/2;
	love.graphics.setColour(newCol);
	love.graphics.draw(blockBrokenSprite, x, y, 0, w / self.width, h / self.height);
	love.graphics.setColour(255, 255, 255, 128);
	love.graphics.rectangle("fill", 0, self.y + self.height/2 - (self.height/2)*(progress), love.graphics.getWidth(), self.height * (progress));
	love.graphics.rectangle("fill", self.x + self.width/2 - (self.width/2)*(progress), 0, self.width * (progress), love.graphics.getHeight());
end
