Paddle = Class('Paddle', GameObject);

function Paddle:initialize()
	GameObject:initialize();
	self.width = 160;
	self.height = 96;
	self.x = (love.graphics.getWidth() - self.width)/2;
	self.y = love.graphics.getHeight() - 80;
	self.colour = colour;
	self.scale = 1;
    self.body = love.physics.newBody(world, self.x + self.width/2, self.y + self.height/2);
	local points = {};
	for i = 0, 6 do
		local dir = 135 - (90*(i/6));
		local x = math.cos(math.rad(dir)) * self.width/2;
		local y = self.height/2-math.sin(math.rad(dir)) * self.height;
		table.insert(points, x);
		table.insert(points, y);
	end
	table.insert(points, 0);
	table.insert(points, 0);
	self.shape = love.physics.newPolygonShape( points);
    self.fixture = love.physics.newFixture(self.body, self.shape);
	self.fixture:setUserData({type = "paddle", object = self});
	self.image = love.graphics.newImage("images/paddle.png");
end

function Paddle:destroy()
	self.body:destroy();
end

function Paddle:draw()
	love.graphics.setColour({255, 255, 255});
	love.graphics.draw(self.image, self.x + self.width/2, self.y + self.height/4, 0, self.scale, self.scale, self.image:getWidth()/2, self.image:getHeight()/2);
end

function Paddle:update(dt)
	if love.keyboard.isDown("left") then
		self.x = self.x - 750 * dt;
		self.x = math.max(0, self.x);
		self.body:setPosition(self.x + self.width/2, self.y + self.height/2);
	end
	if love.keyboard.isDown("right") then
		self.x = self.x + 750 * dt;
		self.x = math.min(love.graphics.getWidth() - self.width, self.x);
		self.body:setPosition(self.x + self.width/2, self.y + self.height/2);
	end
end

function Paddle:keypressed(key)
	if key == "a" then
		table.insert(objList, Congratulations:new());
	end
end
