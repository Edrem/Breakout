BackgroundGear = Class("BackgroundGear", GameObject)

local function angleTween(obj)
	obj.angle = 0;
	Flux.to(obj, 16, {angle = 360}):ease("linear"):oncomplete(angleTween);
end

function BackgroundGear:initialize(locked)
	self.x = love.graphics.getWidth()*0.80;
	self.y = love.graphics.getHeight()/3;
	self.depth = -1;
	self.scale = 1;
	self.gear1 = love.graphics.newImage("images/gear1.png");
	self.gear2 = love.graphics.newImage("images/gear2.png");
	self.canvas = love.graphics.newCanvas(love.graphics.getWidth(), love.graphics.getHeight());
	angleTween(self);
end

function BackgroundGear:draw()
	love.graphics.setCanvas(self.canvas);
	love.graphics.clear();
	love.graphics.setColor(128, 128, 128);
	for i = 0, 60, 10 do
		love.graphics.draw(self.gear1, self.x, self.y + i, math.rad(self.angle*2 + 22.5), self.scale, self.scale, self.gear1:getWidth()/2, self.gear1:getHeight()/2);
		love.graphics.draw(self.gear2, self.x - 180 * 4, self.y + i, math.rad(-self.angle), self.scale, self.scale, self.gear2:getWidth()/2, self.gear2:getHeight()/2);
		love.graphics.draw(self.gear1, self.x, self.y + 116*4 + i, math.rad(-self.angle*2), self.scale, self.scale, self.gear1:getWidth()/2, self.gear1:getHeight()/2);
	end
	love.graphics.setColor(255, 255, 255);
	love.graphics.draw(self.gear1, self.x, self.y, math.rad(self.angle*2 + 22.5), self.scale, self.scale, self.gear1:getWidth()/2, self.gear1:getHeight()/2);
	love.graphics.draw(self.gear2, self.x - 180 * 4, self.y, math.rad(-self.angle), self.scale, self.scale, self.gear2:getWidth()/2, self.gear2:getHeight()/2);
	love.graphics.draw(self.gear1, self.x, self.y + 116*4, math.rad(-self.angle*2), self.scale, self.scale, self.gear1:getWidth()/2, self.gear1:getHeight()/2);
	love.graphics.setCanvas();
	love.graphics.setColour(64, 64, 64, 255);
	love.graphics.draw(self.canvas);
end
