Ball = Class("Ball", GameObject)

function Ball:initialize(x, y, locked)
	self.locked = locked;
	self.paddle = object_find(Paddle)[1];
	self.size = 12;
	self.launchAngle = 80;
	self.x = self.paddle.width/2;
	self.y = -self.size - 10;
	self.scale = 1;
	self.colourTimer = 0;
	self.colour = {255, 255, 255};
	self.setSpeed = 750;

    self.ball = {}
    self.ball.body = love.physics.newBody(world, love.graphics.getWidth()/2, love.graphics.getHeight()+100, "dynamic")
    self.ball.shape = love.physics.newCircleShape( self.size);
    self.ball.fixture = love.physics.newFixture(self.ball.body, self.ball.shape, 1)
    self.ball.fixture:setRestitution(1);
    self.ball.fixture:setFriction(0);
	self.ball.fixture:setUserData({ball = true, object = self})
    self.ball.body:setBullet(true);
    self.ball.body:setFixedRotation(true);

	if not locked then
		self.x = x;
		self.y = y;
		self.ball.body:setPosition(self.x, self.y);
	end
end

function Ball:destroy()
	self.ball.body:destroy();
end

function Ball:update(dt)
	local x, y = self.ball.body:getLinearVelocity();
	local speed = math.sqrt( math.pow(x, 2) + math.pow(y, 2) );
	if speed < self.setSpeed then
		local angle = math.atan2( -y, x);
		--self.ball.body:applyForce(math.cos(angle) * 1000, -math.sin(angle) * 1000);
	end
	if speed > self.setSpeed*1.1 then
		local angle = math.rad(180) - math.atan2( -y, x);
		--self.ball.body:applyForce(math.cos(angle) * 1000, -math.sin(angle) * 1000);
	end
	if self.colourTimer > 0 then
		self.colourTimer = self.colourTimer - (dt);
	end
	if self.locked then
		local x = self.paddle.x + self.x;
		local y = self.paddle.y + self.y;
		self.ball.body:setPosition(x, y);
	end
	if self.ball.body:getY() > love.graphics.getHeight() + self.size and not self.locked then
		instance_destroy(self);
		ballCount = ballCount - 1;
		if ballCount <= 0 and not masterObj then
			table.insert(objList, GameOver:new());
		end
	end
end

function Ball:draw()
	local scale = (self.size*2) / ballSprite:getWidth() * self.scale;
	if self.colourTimer > 0 then
		love.graphics.setCanvas(SplatCanvas);
		local colour = shallowCopy(self.colour);
		colour[4] = 255 * self.colourTimer;
		love.graphics.setColor(colour);
		love.graphics.circle( "fill", self.ball.body:getX(), self.ball.body:getY(), self.size * 0.66);
		love.graphics.setCanvas();
		love.graphics.setColor(Util:mixColour( self.colour, {255, 255, 255}, 1 - self.colourTimer/2));
	end
	love.graphics.draw(ballSprite, self.ball.body:getX(), self.ball.body:getY(), 0, scale * self.scale, scale * self.scale, ballSprite:getWidth()/2, ballSprite:getHeight()/2);
end

function Ball:keypressed(key)
	if key == "space" and self.locked then
		self.x = self.paddle.x + self.x;
		self.y = self.paddle.y + self.y;
		self.ball.body:setPosition(self.x, self.y);
		local angle_x = math.cos(math.rad(self.launchAngle)) * 2500;
		local angle_y =-math.sin(math.rad(self.launchAngle)) * 2500;

		self.ball.body:applyLinearImpulse(angle_x, angle_y);
		self.locked = false;
	end
end
