PointScore = Class("PointScore", GameObject)

local function destroyPoint(obj)
	instance_destroy(obj);
end

function PointScore:initialize(x, y, value)
	self.x = x;
	self.y = y;
	self.value = tostring(value);
	self.scale = 0.4 + (value / 100)/40;
	self.alpha = 255;
	self.depth = 2;
	Flux.to(self, 2, {y = self.y - 80, alpha = 0}):oncomplete(destroyPoint);
end

function PointScore:draw()
	love.graphics.setFont(pointFont);
	love.graphics.setColor(0, 0, 0, self.alpha);
	local offset = 10 * self.scale;
	local offx = pointFont:getWidth(self.value) * self.scale / 2;
	local offy = pointFont:getHeight() * self.scale / 2;
	local fuckup = 30 * self.scale; -- Bullshit font problems
	love.graphics.rectangle("fill", self.x - offx - offset, self.y - offy - offset + fuckup, pointFont:getWidth(self.value)*self.scale + offset, pointFont:getHeight()*self.scale - fuckup);

	love.graphics.setColor(255, 255, 255, self.alpha);
	love.graphics.print(self.value, self.x - offx, self.y - offy, 0, self.scale, self.scale);
end
