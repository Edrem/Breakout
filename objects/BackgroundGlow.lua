BackgroundGlow = Class("BackgroundGlow", GameObject)

local colour = {};
colour[1] = {255, 255, 255};
colour[2] = {255, 0, 0};
colour[3] = {0, 255, 0};
colour[4] = {0, 0, 255};
colour[5] = {255, 255, 0};
colour[6] = {255, 0, 255};
colour[7] = {0, 255, 255};
colour[8] = {255, 128, 128};
colour[9] = {128, 255, 128};
colour[10] = {128, 128, 255};
colour[11] = {255, 255, 128};
colour[12] = {128, 255, 255};
colour[13] = {128, 255, 128};

local function tweenAlpha(obj)
	if obj.alpha == 0 then
		local pos = math.floor(math.random(13));
		obj.colour = Util:mixColour(colour[pos], {0, 0, 0}, 0.75);
		Flux.to(obj, 0.8, {alpha = 1.5}):delay(0.2):oncomplete(tweenAlpha);
	else
		Flux.to(obj, 0.8, {alpha = 0}):oncomplete(tweenAlpha);
	end
end

function BackgroundGlow:initialize()
	self.alpha = 0;
	tweenAlpha(self);
end

function BackgroundGlow:draw()
    for y = 0, love.graphics.getHeight() + 120, 209 do
    	for x = 0, love.graphics.getWidth(), 120 do
			self.colour[4] = (self.alpha - y/(love.graphics.getHeight()+120)) * 255;
			love.graphics.setColour(self.colour);
    		love.graphics.draw(tile, x - 60, y -70);
    	end
    end
    for y = 35, love.graphics.getHeight(), 209 do
    	for x = 0, love.graphics.getWidth(), 120 do
			self.colour[4] = (self.alpha - y/love.graphics.getHeight()) * 255;
			love.graphics.setColour(self.colour);
    		love.graphics.draw(tile, x, y);
    	end
    end
end
