GameOver = Class("GameOver", GameObject)

function GameOver:initialize()
	GameObject:initialize();
	masterObj = self;
	self.font = love.graphics.newFont("fonts/kenpixelblocks.ttf", 80);
	self.y = (love.graphics.getHeight() - self.font:getHeight()*2)/2;
	local sound = love.audio.newSource("audio/gameover.ogg");
	sound:play();
end

function GameOver:drawGui()
	love.graphics.setColour(0, 0, 0, 128);
	love.graphics.rectangle("fill", 0, 0, love.graphics.getWidth(), love.graphics.getHeight());
	love.graphics.setFont(self.font);
	love.graphics.setColour(255, 255, 255);
	love.graphics.printf("Game Over", 0, self.y, love.graphics.getWidth(), "center");
end

function GameOver:keypressed(key)
	if key == "return" or key == "space" then
		instance_destroy(self);
		gate:gotoRoom("MainMenu");
	end
end
